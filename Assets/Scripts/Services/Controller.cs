﻿using UnityEngine;

public class Controller : MonoBehaviour
{
    [SerializeField] private SOSetup _setup;
    public static Bounds Bounds;
    public static Scores Scores;
    public static SOSetup Setup;
    public static SaveLoadLeaderboard Leaderboard;

    private void Awake()
    {
        Setup = _setup;
        Bounds = GetComponent<Bounds>();
        Scores = GetComponent<Scores>();
        Leaderboard = GetComponent<SaveLoadLeaderboard>();
    }
}
