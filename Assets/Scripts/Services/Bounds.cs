﻿using UnityEngine;

public class Bounds : MonoBehaviour
{
    private Vector2 _minPosition;
    private Vector2 _maxPosition;

    public float GetMinX => _minPosition.x;
    public float GetMinY => _minPosition.y;
    public float GetMaxX => _maxPosition.x;
    public float GetMaxY => _maxPosition.y;

    private void Awake()
    {
        _minPosition = Camera.main.ViewportToWorldPoint (new Vector2 (0,0));
        _maxPosition = Camera.main.ViewportToWorldPoint (new Vector2 (1,1));
    }
}
