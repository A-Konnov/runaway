﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using Newtonsoft.Json;



public class SaveLoadLeaderboard : MonoBehaviour
{
    private string _path;
    private List<string[]> _leaderboard = new List<string[]>();

    public List<string[]> GetLeaderboard => _leaderboard;

    private void Awake()
    {
        GetPath();
        
        if (File.Exists(_path))
        {
            Load();
        }
    }


    private void GetPath()
    {
#if (UNITY_ANDROID || UNITY_IOS) && !UNITY_EDITOR
        _path = Path.Combine(Application.persistentDataPath, "Leaderboard.json");
#else
        _path = Path.Combine(Application.dataPath, "Leaderboard.json");
#endif
    }

    private void Load()
    {
        _leaderboard = JsonConvert.DeserializeObject<List<string[]>>(File.ReadAllText(_path));
    }

    public void AddResult(string name, int scores)
    {
        _leaderboard.Add(new string[] { name, $"{scores}" });
        Sorting();
        Save();
    }

    private void Save()
    {
        string jsonLeaderboard = JsonConvert.SerializeObject(_leaderboard);
        File.WriteAllText(_path, jsonLeaderboard);
    }
    
    private void Sorting()
    {
        ScoreComparer sc = new ScoreComparer();
        _leaderboard.Sort(sc);
    }
}

class ScoreComparer : IComparer<string[]>
{
    public int Compare(string[] o1, string[] o2)
    {
        int a = Convert.ToInt32(o1[1]);
        int b = Convert.ToInt32(o2[1]);
 
        if (a > b)
        {
            return -1;
        }
        else if (a < b)
        {
            return 1;
        }
 
        return 0;
    }
}