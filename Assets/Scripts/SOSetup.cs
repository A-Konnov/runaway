﻿using UnityEngine;
using System;

[CreateAssetMenu(fileName = "Setup", menuName = "Setup Game")]
public class SOSetup : ScriptableObject, ISerializationCallbackReceiver
{
    [Header("Player")]
    public float SpeedPlayer = 4f;

    [Header("Enemy")]
    public float StartSpeedEnemy = 2f;
    public float DelaySpeedIncrease = 2f;
    public float SpeedIncrease = 1f;
    [NonSerialized] public float CurSpeedEnemy;

    [Header("Spawner")]
    public float DelaySpawnEnemys = 5f;
    public int CountEnemySpawn = 3;
    public GameObject EnemyPrefab;

    [Header("Scores")]
    public float FrequencyAddScore = 1f;
    public int AddScoreInOnFrequency = 1;

    [Header("Bonuses")]
    public float FrequencyAppearanceBonus = 5f;

    public void OnBeforeSerialize(){}

    public void OnAfterDeserialize()
    {
        CurSpeedEnemy = StartSpeedEnemy;
    }
}
