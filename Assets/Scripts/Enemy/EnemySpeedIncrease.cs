﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpeedIncrease : MonoBehaviour
{
    private WaitForSeconds _delay;
    private float _speedAdd;
    private float _curSpeed;

    public float GetCurSpeed => _curSpeed;

    private void Start()
    {
        _curSpeed = Controller.Setup.StartSpeedEnemy;
        _speedAdd = Controller.Setup.SpeedIncrease;
        _delay = new WaitForSeconds(Controller.Setup.DelaySpeedIncrease);
        StartCoroutine(SpeedIncrease());
    }

    private IEnumerator SpeedIncrease()
    {
        while (true)
        {
            yield return _delay;
            _curSpeed += _speedAdd;
        }
    }
}
