﻿using UnityEngine;

public class Chase : MonoBehaviour
{
    private Transform _target;
    private float _speed;
    private float _dt;

    public float SetSpeed
    {
        set => _speed = value;
    }
    
    private void Start()
    {
        _dt = Time.deltaTime;
        _target = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
    }

    private void Update()
    {
        if (_target != null)
        {
            transform.up = _target.position - transform.position;
            transform.position = Vector2.MoveTowards(transform.position, _target.position, _speed * _dt);
        }
    }
}
