﻿using System.Collections;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    private GameObject _enemyPrefab;
    private Bounds _bounds;
    private int _countEnemys;
    private WaitForSeconds _delay;
    private Vector3 _position = Vector3.zero;
    private Transform _player;
    private EnemySpeedIncrease _enemySpeedIncrease;

    private void Start()
    {
        _player = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
        _enemySpeedIncrease = GetComponent<EnemySpeedIncrease>();
        _enemyPrefab = Controller.Setup.EnemyPrefab;
        _countEnemys = Controller.Setup.CountEnemySpawn;
        _bounds = Controller.Bounds;
        
        _delay = new WaitForSeconds(Controller.Setup.DelaySpawnEnemys);
        StartCoroutine(SpawnRelease());
    }

    private IEnumerator SpawnRelease()
    {
        Vector3 heading;
        float distance;

        while (true)
        {
            if (_player == null)
                yield break;
            
            for (int i = 0; i < _countEnemys;)
            {
                _position.x = Random.Range(_bounds.GetMinX, _bounds.GetMaxX);
                _position.y = Random.Range(_bounds.GetMinY, _bounds.GetMaxY);
                
                heading = _player.position - _position;
                distance = heading.sqrMagnitude;
                
                if (distance > 1f)
                {
                    var enemy = Instantiate(_enemyPrefab, _position, Quaternion.identity);
                    enemy.GetComponent<Chase>().SetSpeed = _enemySpeedIncrease.GetCurSpeed;
                    i++;
                }
            }

            yield return _delay;
        }
    }
}