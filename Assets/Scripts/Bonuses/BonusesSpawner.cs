﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BonusesSpawner : MonoBehaviour
{
    [SerializeField] private GameObject[] _bonusesVariants;
    private List<List<Transform>> _pullBonuses;
    private WaitForSeconds _delay;
    private Vector3 _position = Vector3.zero;
    private Bounds _bounds;

    private void Start()
    {
        _bounds = Controller.Bounds;
        _pullBonuses = new List<List<Transform>>();
        for (int i = 0; i < _bonusesVariants.Length; i++)
        {
            var variantPull = new List<Transform>();
            _pullBonuses.Add(variantPull);
        }
        
        _delay = new WaitForSeconds(Controller.Setup.FrequencyAppearanceBonus);
        StartCoroutine(AppearanceBonus());
    }

    private IEnumerator AppearanceBonus()
    {
        while (true)
        {
            yield return _delay;
            AddBonus();
        }
    }

    private void AddBonus()
    {
        int variant = Random.Range(0, _bonusesVariants.Length);
        
        if (_pullBonuses[variant].Count > 0)
        {
            foreach (var bonus in _pullBonuses[variant])
            {
                if (!bonus.gameObject.activeSelf)
                {
                    bonus.gameObject.SetActive(true);
                    PositionBonus(bonus);
                    return;
                }
            }
        }

        CreateBonus(variant);
    }

    private void CreateBonus(int variant)
    {
        var bonus = Instantiate(_bonusesVariants[variant]).GetComponent<Transform>();
        PositionBonus(bonus);
        _pullBonuses[variant].Add(bonus);
    }

    private void PositionBonus(Transform bonus)
    {
        _position.x = Random.Range(_bounds.GetMinX, _bounds.GetMaxX);
        _position.y = Random.Range(_bounds.GetMinY, _bounds.GetMaxY);
        bonus.position = _position;
    }
}


