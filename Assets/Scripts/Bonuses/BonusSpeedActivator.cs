﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BonusSpeedActivator : ABonusActivator
{
    [SerializeField] private SOBonusSpeed _bonusSpeedData;
    
    protected override void Activate(GameObject target)
    {
        var bonus = target.AddComponent<BonusSpeed>();
        bonus.SetBonusSpeedData = _bonusSpeedData;
    }
}
