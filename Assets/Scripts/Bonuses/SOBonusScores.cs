﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "BonusScoreData", menuName = "Bonuses Data/Score")]
public class SOBonusScores : ScriptableObject
{
    [SerializeField] private int _scoresAdd;

    public int GetScoresAdd => _scoresAdd;
}
