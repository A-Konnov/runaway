﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ABonus : MonoBehaviour
{
    protected abstract void BonusOn();
    protected abstract void BonusOff();

    private void Start()
    {
        BonusOn();
    }
}
