﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ABonusActivator : MonoBehaviour
{
    [SerializeField] private float _lifeTime = 10f;
    private WaitForSeconds _delay;
    private Coroutine _lifeTimeCor;

    protected abstract void Activate(GameObject target);
    
    private void Awake()
    {
        _delay = new WaitForSeconds(_lifeTime);
    }

    private void OnEnable()
    {
        _lifeTimeCor = StartCoroutine(LifeTime());
    }

    private IEnumerator LifeTime()
    {
        yield return _delay;
        gameObject.SetActive(false);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            StopCoroutine(_lifeTimeCor);
            gameObject.SetActive(false);
            Activate(other.gameObject);
        }
    }
}
