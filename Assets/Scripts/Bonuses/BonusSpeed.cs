﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BonusSpeed : ABonus
{
    private SOBonusSpeed _bonusSpeedData;
    private Move _player;

    public SOBonusSpeed SetBonusSpeedData
    {
        get => _bonusSpeedData;
        set => _bonusSpeedData = value;
    }


    protected override void BonusOn()
    {
        _player = GetComponent<Move>();
        _player.AddSpeed = _bonusSpeedData.GetSpeedAdd;
        StartCoroutine(LifeTime());
    }

    protected override void BonusOff()
    {
        _player.AddSpeed = _bonusSpeedData.GetSpeedAdd * -1;
        Destroy(this);
    }

    private IEnumerator LifeTime()
    {
        yield return new WaitForSeconds(_bonusSpeedData.GetLifeTimeBonus);
        BonusOff();
    }
}
