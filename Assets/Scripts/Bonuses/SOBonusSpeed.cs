﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "BonusSpeedData", menuName = "Bonuses Data/Speed")]
public class SOBonusSpeed : ScriptableObject
{
    [SerializeField] private float _speedAdd;
    [SerializeField] private float _lifeTimeBonus;

    public float GetLifeTimeBonus => _lifeTimeBonus;
    public float GetSpeedAdd => _speedAdd;
}
