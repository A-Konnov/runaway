﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BonusScoresActivator : ABonusActivator
{
    [SerializeField] private SOBonusScores _bonusScoreData;

    protected override void Activate(GameObject target)
    {
        Controller.Scores.AddScores = _bonusScoreData.GetScoresAdd;
        gameObject.SetActive(false);
    }
}
