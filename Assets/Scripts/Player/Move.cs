﻿using UnityEngine;

public class Move : MonoBehaviour
{
    private Vector3 _target;
    private Camera _camera;
    private float _speed;
    private float _dt;
    private bool _isStop = false;

    public float AddSpeed
    {
        set => _speed += value;
    }

    private void Start()
    {
        _speed = Controller.Setup.SpeedPlayer;
        _dt = Time.deltaTime;
        _camera = Camera.main;
    }

    private void Update()
    {
        if (_isStop)
            return;
        
        if (Input.GetMouseButtonDown(0))
        {
            _target = _camera.ScreenToWorldPoint(Input.mousePosition);
            _target.z = 0;
            transform.up = _target - transform.position;
        }
        
        transform.position = Vector2.MoveTowards(transform.position, _target, _speed * _dt);
    }

    public void Stop()
    {
        _isStop = true;
    }
}
