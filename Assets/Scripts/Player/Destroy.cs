﻿using UnityEngine;
using UnityEngine.Events;

public class Destroy : MonoBehaviour
{
    public UnityEvent DestroyEvent = new UnityEvent();
    
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Enemy"))
        {
            Time.timeScale = 0;
            DestroyEvent.Invoke();
        }
    }
}
