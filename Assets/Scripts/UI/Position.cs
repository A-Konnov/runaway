﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Position : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _number;
    [SerializeField] private TextMeshProUGUI _name;
    [SerializeField] private TextMeshProUGUI _score;

    public void Place(int number, string name, string score)
    {
        _number.text = $"{number}";
        _name.text = name;
        _score.text = score;
    }
}
