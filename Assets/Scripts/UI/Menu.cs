﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
    public void OnBtnStart()
    {
        Time.timeScale = 1f;
        SceneManager.LoadSceneAsync(1);
    }
    
    public void OnBtnLeaderboard()
    {
        SceneManager.LoadSceneAsync(2);
    }
    
    public void OnBtnStartMenu()
    {
        SceneManager.LoadSceneAsync(0);
    }
    
}
