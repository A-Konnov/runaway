﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Scores : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _scoresTxt;
    private Coroutine _сountScores;
    private int _scores = 0;
    private int _scoreInOnFrequency;

    public int AddScores
    {
        set
        {
            _scores += value;
            _scoresTxt.text = $"{_scores}";
        }
    }

    public int GetScores => _scores;
    
    private void Start()
    {
        _сountScores = StartCoroutine(СountScores());
        _scoreInOnFrequency = Controller.Setup.AddScoreInOnFrequency;
        _scoresTxt.text = $"{_scores}";
    }

    private IEnumerator СountScores()
    {
        var delay = new WaitForSeconds(Controller.Setup.FrequencyAddScore);
        
        while (true)
        {
            yield return delay;
            AddScores = _scoreInOnFrequency;
        }
    }
}
