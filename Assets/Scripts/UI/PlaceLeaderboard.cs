﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaceLeaderboard : MonoBehaviour
{
    [SerializeField] private GameObject _prefab;
    [SerializeField] private Transform _content;
    private SaveLoadLeaderboard _saveLoadLeaderboard;

    private void Start()
    {
        _saveLoadLeaderboard = GetComponent<SaveLoadLeaderboard>();

        for (int i = 0; i < _saveLoadLeaderboard.GetLeaderboard.Count; i++)
        {
            var position = Instantiate(_prefab, _content);
            position.GetComponent<Position>().Place(
                i+1, 
                _saveLoadLeaderboard.GetLeaderboard[i][0], 
                _saveLoadLeaderboard.GetLeaderboard[i][1]);
        }
    }
}
