﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TimePassed : MonoBehaviour
{
    private TextMeshProUGUI _timeTxt;
    private int _time = 0;

    private void Start()
    {
        _timeTxt = GetComponent<TextMeshProUGUI>();
        _timeTxt.text = $"Time: {_time}";
        StartCoroutine(Timer());
    }

    private IEnumerator Timer()
    {
        var delay = new WaitForSeconds(1f);
        while (true)
        {
            yield return delay;
            _time++;
            _timeTxt.text = $"Time: {_time}";
        }
    }
}
