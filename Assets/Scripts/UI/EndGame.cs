﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class EndGame : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _scoreTxt;
    [SerializeField] private TMP_InputField _input;
    private Animator _animator;

    private void Start()
    {
        _animator = GetComponent<Animator>();
    }

    public void OpenWindow()
    {
        _animator.SetBool("Open", true);
        _scoreTxt.text = $"{Controller.Scores.GetScores}";
    }

    public void OnBtnSaveResult()
    {
        Debug.Log(_input.text);
        Controller.Leaderboard.AddResult(_input.text, Controller.Scores.GetScores);
    }
}
